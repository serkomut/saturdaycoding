﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using MyTools.Models;

namespace MyTools.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Split()
        {
            return View();
        }

        public ActionResult MailSender()
        {
            return View();
        }

        public ActionResult SendMail()
        {
            return View();
        }

	}
}