﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTools.Models
{
    public class Mail
    {
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
        public string GonderilecekMail { get; set; }
        public string Konu { get; set; }
        public string Mesaj { get; set; }
        public string GondericiMail { get; set; }
    }
}