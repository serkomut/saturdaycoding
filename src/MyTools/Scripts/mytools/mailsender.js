﻿$(document).ready(function () {
    mailSender.init();
});
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
var mailSender = function () {
    var readfile = function() {
        var file = $('#listinput').val();
        var uri = '/Content/maillist.txt';
        $.get(uri, function (data) {
            var lines = data.split("\n");
            $('#resultCount').html('Toplam '+lines.length +' mail adresi sayıldı.');
            $.each(lines, function(n, elem) {
                $("#maillist ul").append($('<li>').html(elem));
            });
            
        });
    };
    var sendmail = function() {

    };

    var selectAll = function() {
        $('#selectAll').click(function() {
            $('input[name=mail]:checkbox').prop('checked', this.checked);
        });
    };

    var mailPrepare = function () {
        $('#splitle').click(function () {
            var thead = '<tr>' +
                '<th></th><th>' +
                '<input id="selectAll" type="checkbox" style="float: left;" />' +
                '<label for="selectAll" style="float: left; margin-left: 10px;">Hepsini Seç</label>' +
                '</th><th>' +
                'Mail' +
                '</th></tr>';
            var values = $('#listinput').val().split('\n');
            var webApiUrl = 'http://api.webkomut.com/api/SendMail';
            var message = '';
            var subject = '';
            var sendResult = $('#sendMailResult');
            $('#liste table thead').empty().append(thead);
            $('#liste table tbody').empty();
            $('#resultCount').html('Toplam ' + values.length + ' mail adresi sayıldı.');
            selectAll();
            $.each(values, function (n, mail) {
                if (!isValidEmailAddress(mail)) {
                    $('#liste table tbody').append('<tr style="background-color:red;"><td class="tablefirstrow">' + n + '</td><td class="tablesecondrow"></td><td>' + mail + '</td></tr>');
                    $('#errorCount').append($('<span>').append('Hatalı Mail Satırı: ' + n + '</br>'));
                    //$.ajax()
                } else {
                    $('#liste table tbody').append('<tr><td class="tablefirstrow">' + n + '</td><td class="tablesecondrow"><input id="checkbox' + n + '" name="mail" type="checkbox"/></td><td>' + mail + '</td></tr>');
                    $.ajax({
                        url: webApiUrl,
                        dataType: 'json',
                        data: $.param({
                            mail: mail,
                            message: message,
                            subject:subject
                        }),
                        success: function (result) {
                            sendResult.fadeIn("slow", function () {
                                sendResult.empty();
                                sendResult.append('Mail ' + mail + ' adresine başarıyla gönderildi.');
                                sendResult.delay(3000).hide(400);
                            });
                        },
                        error:function(error) {
                            sendResult.fadeIn("slow", function () {
                                sendResult.empty();
                                sendResult.append('Mail ' + mail + ' gönderilemedi.');
                                sendResult.delay(3000).hide(400);
                            });
                        }
                });
                }
            });

        });
        
    };
    return {
        init: function() {
            sendmail();
            selectAll();
            //readfile();
            mailPrepare();
        }
    };
}();