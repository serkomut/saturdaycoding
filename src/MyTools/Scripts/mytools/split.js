﻿function Spliter() {
            var mails = $('#listinput').val();
            var sp = $('#split').val();
            var row = mails.split(sp);
            var result = $('#resultCount');
            var checkbox = $('#Checkbox1');
            if (mails != "") {
                result.empty();
                result.text('Toplam ' + row.length + ' satır.');
                $('#liste').append($('<img>').attr('src', 'images/input-spinner.gif'));
                $('#liste').empty().append($('<table>'));
                if (checkbox.is(':checked')) {
                    for (var i = 0; i < row.length; i++) {
                        $('#liste table').append('<tr><th style="display:block;">' + i + '</th><td>' + row[i] + '</td></tr>');
                        
                    }
                } else {
                    for (var i = 0; i < row.length; i++) {
                        $('#liste table').append('<tr><th style="display:none;">' + i + '</th><td>' + row[i] + '</td></tr>');
                        
                    }
                }
                checkedEvent.init();
            }

        }

        $(document).ready(function () {
            checkedEvent.init();
        });

        var checkedEvent = function() {
            var chc = function() {
                $('#Checkbox1').click(function() {
                    var tablelist = $('#liste table tr');
                    if (tablelist.size() > 0) {
                        if (this.checked) {
                            $('#liste table tr > th').css('display', 'block');
                        } else {
                            $('#liste table tr > th').css('display', 'none');
                        }
                    }

                });
            };

            return {
                init: function() {
                    chc();
                }
            };
        }();