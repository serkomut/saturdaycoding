﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiSempla.Models
{
    public class UserInfo
    {
        [Key]
        public int InfoId { get; set; }
        public int UserId { get; set; }
        public int InfoType { get; set; }
        public string Content { get; set; }
        public virtual Users Users { get; set; }
    }
}