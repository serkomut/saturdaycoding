﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample.Web.Models
{
    public class Users
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string RealName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

    }
}