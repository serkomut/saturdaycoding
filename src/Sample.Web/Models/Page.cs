﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sample.Web.Models
{
    public class Page
    {
        [Key]
        public int PageId { get; set; }

        public string Content { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}