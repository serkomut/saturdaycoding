﻿using System.Data.Entity;
using System.Web.UI.WebControls;

namespace Sample.Web.Models
{
    public class SimpleContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SimpleContext() : base("name=SimpleContext")
        {

        }

        public DbSet<Page> Pages { get; set; }
       
    
    }
}
