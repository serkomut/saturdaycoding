﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Urun urun1 = new Urun();
            urun1.Id = 1;
            urun1.Ad = "Monitör";
            urun1.Fiyat = 200;

            Urun urun2 = new Urun();
            urun2.Id = 2;
            urun2.Ad = "Klavye";
            urun2.Fiyat = 80;
            Urun.KdvOran = 0.18;


            urun2.ZamYap(0.80);
            urun1.ZamYap(0.50);
            Urun.KdvOranDegisti(0.20);
            Console.WriteLine(urun1.Fiyat);
            Console.WriteLine(urun2.Fiyat);
            Console.ReadKey(true);
        }
    }
}
